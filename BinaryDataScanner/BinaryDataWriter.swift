//
//  BinaryDataWriter.swift
//  BinaryDataScanner
//
//  Created by Frederick Cox on 27/08/2018.
//  Copyright © 2018 Kidslox. All rights reserved.
//

import Foundation

extension BinaryReadable {
    fileprivate func bytes(endian: __CFByteOrder) -> Data {
        var copy = self
        return withUnsafePointer(to: &copy) { (pointer: UnsafePointer<Self>) -> Data in
            var data = Data(bytes: pointer, count: MemoryLayout<Self>.size)
            if Self.ordered && endian.rawValue != UInt32(CFByteOrderGetCurrent()) {
                data = Data(data.reversed())
            }
            return data
        }
    }
}

public class BinaryDataWriter {
    private(set) public var data: Data
    private let endian: __CFByteOrder

    public init(data: Data = Data(), endian: __CFByteOrder) {
        self.data = data
        self.endian = endian
    }

    public func write<T: BinaryReadable>(_ value: T) {
        self.data.append(value.bytes(endian: endian))
    }

    public func append(_ data: Data) {
        self.data.append(data)
    }
}

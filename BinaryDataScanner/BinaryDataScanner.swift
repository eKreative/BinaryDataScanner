//
//  BinaryDataScanner.swift
//  BinaryDataScanner
//
//  Created by Fred Cox on 1/16/18.
//  Copyright © 2018 Kidslox. All rights reserved.
//
// Losely based on https://gist.github.com/davepeck/495ea71f815ce292d6e0
//

import Foundation

public protocol BinaryReadable {
    static var ordered: Bool { get }
}

extension BinaryReadable {
    public static var ordered: Bool {
        return true
    }
}

public enum BinaryDataError: Error {
    case outOfBound
}

public class BinaryDataScanner {
    private let data: Data
    private let endian: __CFByteOrder

    public var remaining: Int {
        return data.count - position
    }

    private(set) public var position: Int = 0

    public init(data: Data, endian: __CFByteOrder) {
        self.data = data
        self.endian = endian
    }

    public func read<T: BinaryReadable>() throws -> T {
        return value(data: try data(count: MemoryLayout<T>.size))
    }

    public func peekRead<T: BinaryReadable>() throws -> T {
        return value(data: try peekData(count: MemoryLayout<T>.size))
    }

    public func value<T: BinaryReadable>(data: Data) -> T {
        var subdata = data
        if T.ordered && endian.rawValue != UInt32(CFByteOrderGetCurrent()) {
            subdata = Data(subdata.reversed())
        }

        let val = subdata.withUnsafeBytes { (pointer: UnsafePointer<T>) in
            return pointer.pointee
        }

        return val
    }

    // swiftlint:disable:next identifier_name
    public func skip(to: Int) {
        position = to
    }

    // swiftlint:disable:next identifier_name
    public func advance(by: Int) {
        position += by
    }

    public func data(count: Int) throws -> Data {
        let subdata = try peekData(count: count)
        position += count

        return subdata
    }

    public func peekData(count: Int) throws -> Data {
        if remaining < count {
            throw BinaryDataError.outOfBound
        }

        let subdata = data[self.position..<self.position + count]

        return subdata
    }

    public func remainingData() throws -> Data {
        return try data(count: remaining)
    }
}

extension UInt8: BinaryReadable {}

extension UInt16: BinaryReadable {}

extension UInt32: BinaryReadable {}

extension UInt64: BinaryReadable {}

extension BinaryDataScanner {
    public func read8() throws -> UInt8 {
        return try read()
    }

    public func read16() throws -> UInt16 {
        return try read()
    }

    public func read32() throws -> UInt32 {
        return try read()
    }

    public func read64() throws -> UInt64 {
        return try read()
    }

    public func peekRead4() throws -> UInt8 {
        let value = try peekRead8()
        return value >> 4
    }

    public func peekRead8() throws -> UInt8 {
        return try peekRead()
    }

    public func peekRead16() throws -> UInt16 {
        return try peekRead()
    }

    public func peekRead32() throws -> UInt32 {
        return try peekRead()
    }

    public func peekRead64() throws -> UInt64 {
        return try peekRead()
    }
}

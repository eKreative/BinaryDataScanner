# BinaryDataScanner

A library for reading and writing binary data

### Releasing a new version

1. Check passing pod lint, `pod lib lint BinaryDataScanner.podspec`
2. Update version in podspec
3. Commit and tag new version
4. Push the tag
5. `pod trunk push BinaryDataScanner.podspec`

//
//  BinaryDataScanner_macos.h
//  BinaryDataScanner-macos
//
//  Created by Frederick Cox on 27/08/2018.
//  Copyright © 2018 Kidslox. All rights reserved.
//

#import <Cocoa/Cocoa.h>

//! Project version number for BinaryDataScanner_macos.
FOUNDATION_EXPORT double BinaryDataScanner_macosVersionNumber;

//! Project version string for BinaryDataScanner_macos.
FOUNDATION_EXPORT const unsigned char BinaryDataScanner_macosVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BinaryDataScanner_macos/PublicHeader.h>



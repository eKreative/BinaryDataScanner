Pod::Spec.new do |s|

  s.name         = "BinaryDataScanner"
  s.version      = "1.0.3"
  s.summary      = "A lib for parsing binary data."

  s.homepage     = "https://gitlab.com/eKreative/BinaryDataScanner"
  s.license      = "Closed source"
  s.author       = { "Fred Cox" => "frederick.j.cox@gmail.com" }

  s.platform     = :ios, "9.3"

  s.ios.deployment_target = "9.3"
  s.osx.deployment_target = "10.10"
  s.source       = { :git => "https://gitlab.com/eKreative/BinaryDataScanner.git", :tag => "#{s.version}" }


  s.source_files  = "BinaryDataScanner/**/*.swift"
  s.swift_version = "4.0"
end

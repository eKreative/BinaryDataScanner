//
//  BinaryDataScannerTests.swift
//  BinaryDataScannerTests
//
//  Created by Frederick Cox on 27/08/2018.
//  Copyright © 2018 Kidslox. All rights reserved.
//

@testable import BinaryDataScanner
import XCTest

class BinaryDataScannerTests: XCTestCase {

    func testLittleEndian4() throws {
        let data = Data(bytes: [0x45])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderLittleEndian)

        let i = try a.peekRead4()
        XCTAssertEqual(i, 4)
    }

    func tesBigEndian4() throws {
        let data = Data(bytes: [0x45])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.peekRead4()
        XCTAssertEqual(i, 4)
    }

    func testLittleEndian8() throws {
        let data = Data(bytes: [0x01])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderLittleEndian)

        let i = try a.read8()
        XCTAssertEqual(i, 1)
    }

    func testBigEndian8() throws {
        let data = Data(bytes: [0x01])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.read8()
        XCTAssertEqual(i, 1)
    }

    func testLittleEndian16() throws {
        let data = Data(bytes: [0x01, 0x00])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderLittleEndian)

        let i = try a.read16()
        XCTAssertEqual(i, 1)
    }

    func testBigEndian16() throws {
        let data = Data(bytes: [0x00, 0x01])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.read16()
        XCTAssertEqual(i, 1)
    }

    func testLittleEndian32() throws {
        let data = Data(bytes: [0x01, 0x00, 0x00, 0x00])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderLittleEndian)

        let i = try a.read32()
        XCTAssertEqual(i, 1)
    }

    func testBigEndian32() throws {
        let data = Data(bytes: [0x00, 0x00, 0x00, 0x01])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.read32()
        XCTAssertEqual(i, 1)
    }

    func testLittleEndian64() throws {
        let data = Data(bytes: [0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderLittleEndian)

        let i = try a.read64()
        XCTAssertEqual(i, 1)
    }

    func testBigEndian64() throws {
        let data = Data(bytes: [0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.read64()
        XCTAssertEqual(i, 1)
    }

    func testConsecutiveRead() throws {
        let data = Data(bytes: [0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x02])
        let a = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)

        let i = try a.read32()
        XCTAssertEqual(i, 1)

        let j = try a.read32()
        XCTAssertEqual(j, 2)
    }

    func testReadingBits() throws {
        let data = Data(bytes: [0x12, 0x34, 0x56, 0x78])
        let reader = BinaryDataScanner(data: data, endian: CFByteOrderBigEndian)
        let i = try reader.read32()

        let a = i >> 28 // 4
        XCTAssertEqual(a, 0x1)

        let b = (i >> 20) & 0xFF // 8
        XCTAssertEqual(b, 0x23)

        let c = i & 0xFFFFF // 20
        XCTAssertEqual(c, 0x45678)
    }

}

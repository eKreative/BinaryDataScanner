//
//  BinaryDataWriterTests.swift
//  BinaryDataScanner
//
//  Created by Frederick Cox on 27/08/2018.
//  Copyright © 2018 Kidslox. All rights reserved.
//

@testable import BinaryDataScanner
import XCTest

class BinaryDataWriterTests: XCTestCase {

    func testLittleEndian8() {
        let a = BinaryDataWriter(endian: CFByteOrderLittleEndian)
        a.write(UInt8(1))

        XCTAssertEqual(a.data.count, 1)
        XCTAssertEqual(a.data[0], 1)
    }

    func testBigEndian8() {
        let a = BinaryDataWriter(endian: CFByteOrderBigEndian)
        a.write(UInt8(1))

        XCTAssertEqual(a.data.count, 1)
        XCTAssertEqual(a.data[0], 1)
    }

    func testLittleEndian16() {
        let a = BinaryDataWriter(endian: CFByteOrderLittleEndian)
        a.write(UInt16(1))

        XCTAssertEqual(a.data.count, 2)
        XCTAssertEqual(a.data[0], 1)
        XCTAssertEqual(a.data[1], 0)
    }

    func testBigEndian16() {
        let a = BinaryDataWriter(endian: CFByteOrderBigEndian)
        a.write(UInt16(1))

        XCTAssertEqual(a.data.count, 2)
        XCTAssertEqual(a.data[0], 0)
        XCTAssertEqual(a.data[1], 1)
    }

    func testLittleEndian32() {
        let a = BinaryDataWriter(endian: CFByteOrderLittleEndian)
        a.write(UInt32(1))

        XCTAssertEqual(a.data.count, 4)
        XCTAssertEqual(a.data[0], 1)
        XCTAssertEqual(a.data[1], 0)
        XCTAssertEqual(a.data[2], 0)
        XCTAssertEqual(a.data[3], 0)
    }

    func testBigEndian32() {
        let a = BinaryDataWriter(endian: CFByteOrderBigEndian)
        a.write(UInt32(1))

        XCTAssertEqual(a.data.count, 4)
        XCTAssertEqual(a.data[0], 0)
        XCTAssertEqual(a.data[1], 0)
        XCTAssertEqual(a.data[2], 0)
        XCTAssertEqual(a.data[3], 1)
    }

    func testLittleEndian64() {
        let a = BinaryDataWriter(endian: CFByteOrderLittleEndian)
        a.write(UInt64(1))

        XCTAssertEqual(a.data.count, 8)
        XCTAssertEqual(a.data[0], 1)
        XCTAssertEqual(a.data[1], 0)
        XCTAssertEqual(a.data[2], 0)
        XCTAssertEqual(a.data[3], 0)
        XCTAssertEqual(a.data[4], 0)
        XCTAssertEqual(a.data[5], 0)
        XCTAssertEqual(a.data[6], 0)
        XCTAssertEqual(a.data[7], 0)
    }

    func testBigEndian64() {
        let a = BinaryDataWriter(endian: CFByteOrderBigEndian)
        a.write(UInt64(1))

        XCTAssertEqual(a.data.count, 8)
        XCTAssertEqual(a.data[0], 0)
        XCTAssertEqual(a.data[1], 0)
        XCTAssertEqual(a.data[2], 0)
        XCTAssertEqual(a.data[3], 0)
        XCTAssertEqual(a.data[4], 0)
        XCTAssertEqual(a.data[5], 0)
        XCTAssertEqual(a.data[6], 0)
        XCTAssertEqual(a.data[7], 1)
    }

    func writeConsecutive() {
        let a = BinaryDataWriter(endian: CFByteOrderBigEndian)
        a.write(UInt8(1))
        a.write(UInt8(2))

        XCTAssertEqual(a.data.count, 2)
        XCTAssertEqual(a.data[0], 1)
        XCTAssertEqual(a.data[1], 2)
    }

    func testWritingBits() throws {
        let writer = BinaryDataWriter(endian: CFByteOrderBigEndian)

        let a = UInt8(0x1) // 4
        let b = UInt8(0x23) // 8
        let c = UInt32(0x45678) //20

        let together = UInt32(a) << 28 | (UInt32(b) & 0xFF) << 20 | c & 0xFFFFF

        writer.write(together)
        XCTAssertEqual(writer.data.count, 4)
        XCTAssertEqual(writer.data[0], 0x12)
        XCTAssertEqual(writer.data[1], 0x34)
        XCTAssertEqual(writer.data[2], 0x56)
        XCTAssertEqual(writer.data[3], 0x78)
    }
}
